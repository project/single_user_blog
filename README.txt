Before installing:
This profile relies on the Token, Pathauto, Inline, and Views modules (Drupal 5.x versions).
You can download these at:
http://drupal.org/project/token
http://drupal.org/project/inline
http://drupal.org/project/views
http://drupal.org/project/pathauto

Unzip these folders and place them into your sites/all/modules folder (Create the modules folder if it does not exist already). 

To install:
Unzip this folder into your site's /profiles folder. Then go to your site's url and follow the installation instructions for "Blog site".

--Profile created by herenbdy
--herenbdy@gmail.com
--herenbdy.com