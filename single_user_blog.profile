<?php
// Designed by herenbdy: herenbdy@gmail.com

function single_user_blog_profile_modules() {
  return array('block', 'comment', 'filter', 'help', 'menu', 'node', 'system', 'user', 'watchdog', 
  //additional modules
  'inline', 'taxonomy', 'token', 'path', 'pathauto', 'search', 'statistics', 'upload', 'views', 'views_ui');
}

function single_user_blog_profile_details() {
  return array(
    'name' => 'Blog site',
    'description' => 'Select this profile to create a single-user blog website.'
  );
}

function single_user_blog_profile_final() {
	
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st('If you want to add a static page, like a contact page or an about page, use a page.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'post',
      'name' => st('Blog post'),
      'module' => 'node',
      'description' => st('Create a new post to your blog.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
  );
  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }
  $post_help = 'UPDATE {node_type} SET `help` = \'To post an image in the body of a post, attach the image below in the File attachment section, and insert [inline:filename] in your post where you want the image to appear. The default maximum dimensions are set to 1000x1000, you may change this at <a href="admin/settings/inline"> the inline settings page.</a>'. ' \' WHERE CONVERT({node_type}.`type` USING utf8) = \'post\' LIMIT 1;';
  db_query($post_help); // set post help box
  
  
  $time = time();//current time
  $url = url(NULL, NULL, NULL, TRUE);//base url
  
  
  //setup theme
  db_query("INSERT INTO {system} VALUES ('themes/pushbutton/page.tpl.php', 'pushbutton', 'theme', 'themes/engines/phptemplate/phptemplate.engine', 1, 0, 0, -1, 0);");
  variable_set('theme_settings', $theme_settings);
  variable_set('theme_default', 'pushbutton');
  variable_set('site_name', 'My blog');
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);
  
  
  //comments
  variable_set('comment_default_mode', '4');
  variable_set('comment_default_order', '1');
  variable_set('comment_default_per_page', '50');
  variable_set('comment_controls', '3');
  variable_set('comment_anonymous', '1');
  variable_set('comment_subject_field', '1');
  variable_set('comment_preview', '0');
  variable_set('comment_form_location', '1');
  
  
  //Setup recent posts, recent comments, and admin menu blocks
  db_query("INSERT INTO {view_view} VALUES (1, 'posts_recent', 'Recent blog posts', '', 0, '', '', 1, '', 1, '', 1, 'node', 1, 10, '', 0, 0, 0, '', 0, 'tab', '', 0, 1, 'Recent blog posts', 0, '', 1, 0, '', 1, 0, '', 1, 'list', 5, 0, 0, $time, '', 1);");
  db_query("INSERT INTO {view_sort} VALUES (1, 0, 'node.changed', 'DESC', 'normal', NULL);");
  db_query("INSERT INTO {view_filter} VALUES (1, '', 'node.type', 'post', 'OR', '', 0);");
  db_query("INSERT INTO {view_tablefield} VALUES (1, 'node', 'changed', '', 'views_handler_field_date_small', 0, '0', '', 1);");
  db_query("INSERT INTO {view_tablefield} VALUES (1, 'node', 'title', '', 'views_handler_field_nodelink', 0, '0', 'link', 0);");
  db_query("INSERT INTO {blocks} VALUES ('user', '1', 'pushbutton', 1, 0, 'right', 0, 0, 0, '', '');");
  db_query("INSERT INTO {blocks} VALUES ('comment', '0', 'pushbutton', 1, 2, 'right', 0, 0, 0, '', '');");
  db_query("INSERT INTO {blocks} VALUES ('views', 'posts_recent', 'pushbutton', 1, 1, 'right', 0, 0, 0, '', '');");
  db_query("INSERT INTO {blocks_roles} VALUES ('user', '1', '2');"); //make navigation only accessible to admin
  
  
  //Taxonomy
  db_query("INSERT INTO {vocabulary} VALUES (1, 'Topic', '', '', 0, 0, 0, 0, 1, 'taxonomy', 0);");
  db_query("INSERT INTO {vocabulary_node_types} VALUES (1, 'post');");
  
  
  //Upload and inline settings
  variable_set('upload_uploadsize_default', '5');
  variable_set('upload_usersize_default', '100');
  db_query("INSERT INTO {filters} VALUES (1, 'inline', 0, 10);");
  variable_set('upload_post', '1');
  variable_set('upload_inline_post', '0');
  variable_set('inline_link_img', '1');
  variable_set('inline_img_dim', '1000, 1000');
  
  
  //Page views and error reporting
  variable_set('statistics_count_content_views', '1');
  variable_set('site_403', '');
  variable_set('site_404', '');
  variable_set('error_level', '0');
  
  
  //Primary links
  db_query("INSERT INTO {menu} VALUES (2, 0, '', 'Primary links', '', 0, 115);");
  db_query("INSERT INTO {menu} VALUES (62, 2, '', 'Home', 'Return to home page', 0, 118);");
 
  
  //Pathauto settings
  variable_set('pathauto_node_page_pattern', '[title-raw]');
  variable_set('pathauto_node_post_pattern', '[yyyy]/[mm]/[dd]/[title-raw]');
  
  
  //Sets anonymous user permissions and disallows registration
  db_query("UPDATE {permission} SET `rid` = 1, `perm` = 'access comments, post comments, post comments without approval, access content, search content, use advanced search, access statistics, view post access counter, view uploaded files', `tid` = 0 WHERE `rid` = 1;");
  variable_set('user_register', '0');
  variable_set('user_email_verification', '0');
  
  
  //Creates a default account with username and password: admin
  db_query("INSERT INTO {users} VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', 0, 0, 0, '', '', '$time', '$time', 0, 1, NULL, '', '', '', NULL);");
  drupal_set_message("<b>A default account has been created for you, the username is admin, and the password is admin, please log in and change your username/password before posting content or changing settings.</b>");
  $url = url(NULL, NULL, NULL, TRUE);
  $message = 'To log into your site, go to <a href="'.$url.'user">the user log-in page</a>';
  drupal_set_message($message);
}
